import { useEffect } from "react";

/** DocumentTitle component sets the document title. */
function DocumentTitle(props) {
  useEffect(() => {
    document.title = typeof props.children === "string" ? props.children : "";
  }, [props.children]);
  return null;
}

export default DocumentTitle;
