import styled from "styled-components";

const ViewHeading = styled.h3`
  margin-left: 15px;
`;

export default ViewHeading;
