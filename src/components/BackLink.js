import React from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const StyledLink = styled(Link)`
  color: green;
`;

function BackLink(props) {
  return <StyledLink to="/" {...props} />;
}

export default BackLink;
