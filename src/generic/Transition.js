import posed from "react-pose";

const Transition = posed.div({
  visible: { opacity: 1, transition: { duration: 100 } },
  hidden: { opacity: 0 },
});

export default Transition;
