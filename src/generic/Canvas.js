import React, { useState, useLayoutEffect } from "react";

import Transition from "./Transition";

function Canvas({ id }) {
  const [isVisible, setIsVisible] = useState(false);

  useLayoutEffect(() => {
    setIsVisible(true);
  }, []);

  return (
    <Transition pose={isVisible ? "visible" : "hidden"}>
      <canvas id={id} />
    </Transition>
  );
}

export default Canvas;
