import React from "react";
import { BrowserRouter } from "react-router-dom";
import { connect } from "react-redux";
import { ScrollContext } from "react-router-scroll-4";

import AppRouter from "./router";
import SizeErrorView from "./views/SizeErrorView";

function App({ browser }) {
  return (
    <BrowserRouter>
      <ScrollContext>{browser.greaterThan.small ? <AppRouter /> : <SizeErrorView />}</ScrollContext>
    </BrowserRouter>
  );
}

const mapStateToProps = state => ({ browser: state.browser });

export default connect(mapStateToProps)(App);
