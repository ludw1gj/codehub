import { createStore } from "redux";
import { responsiveStoreEnhancer } from "redux-responsive";
import rootReducer from "./reducers/rootReducer";

const store = createStore(rootReducer, responsiveStoreEnhancer);

export default store;
