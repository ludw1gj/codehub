import { SET_TREE_TYPE } from "../actions/types";

const initState = {
  type: "symmetrical",
};

const treeReducer = (state = initState, action) => {
  switch (action.type) {
    case SET_TREE_TYPE:
      return { type: action.payload };
    default:
      return state;
  }
};

export { treeReducer };
