import { combineReducers } from "redux";
import { responsiveStateReducer } from "redux-responsive";

import { treeReducer } from "./treeReducer";

const rootReducer = combineReducers({ tree: treeReducer, browser: responsiveStateReducer });

export default rootReducer;
