import { SET_TREE_TYPE } from "./types";

export const setTreeType = treeTpe => {
  return {
    type: SET_TREE_TYPE,
    payload: treeTpe,
  };
};
