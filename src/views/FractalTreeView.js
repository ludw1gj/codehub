import React from "react";
import { connect } from "react-redux";
import { BinaryFractalTree } from "@ludw1gj/binary-fractal-tree";
import styled from "styled-components";

import DocumentTitle from "components/DocumentTitle";
import ViewHeading from "components/ViewHeading";
import BackLink from "components/BackLink";
import { setTreeType } from "store/actions/treeActions";
import Canvas from "generic/Canvas";
import FlexRow from "generic/FlexRow";
import FlexColumn from "generic/FlexColumn";

const Btn = styled.span`
  display: flex;
  flex-direction: row;
  width: 150px;
  justify-content: center;

  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  &:hover {
    color: green;
    cursor: pointer;
  }
`;

class FractalTreeView extends React.Component {
  state = {
    binaryTree: null,
    canvas: null,
    ctx: null,
    ready: false,
  };

  componentDidMount() {
    const canvas = document.getElementById("canvas");
    const binaryTree = new BinaryFractalTree(canvas, 1000, 625);
    const ctx = canvas.getContext("2d");
    const deps = { binaryTree, canvas, ctx };
    this.setState({ ...deps, ready: true });
    this.newTree({ deps });
  }

  newTree({ deps }) {
    const { ctx, canvas, binaryTree } = deps ? deps : this.state;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    const options = this.generateOptions();
    binaryTree.draw(options);
  }

  toggleTreeType() {
    const treeType = this.props.tree.type === "symmetrical" ? "natural" : "symmetrical";
    this.props.setTreeType(treeType);
  }

  generateOptions() {
    const options = {
      startX: 500,
      startY: 625,
      branchWidth: BinaryFractalTree.generateRandomNumber(15, 8),
      branchWidthDegradation: BinaryFractalTree.generateRandomNumber(0.8, 0.75),
      branchLength: BinaryFractalTree.generateRandomNumber(107, 100),
      branchLengthDegradation: BinaryFractalTree.generateRandomNumber(0.85, 0.81),
      angle: 0,
      changeInAngel: BinaryFractalTree.generateRandomNumber(30, 10),
      type: this.props.tree.type,
      splitProbability: 0.02,
    };
    return options;
  }

  render() {
    return (
      <>
        <DocumentTitle>Binary Fractal Tree</DocumentTitle>
        <FlexRow>
          <FlexColumn>
            <ViewHeading>
              Binary Fractal Tree | <BackLink to="/">Go Back</BackLink>
            </ViewHeading>
          </FlexColumn>
          <FlexColumn>
            <FlexRow style={{ justifyContent: "flex-end" }}>
              <Btn onClick={this.newTree.bind(this)}>
                <h3>New Tree</h3>
              </Btn>
              <Btn onClick={this.toggleTreeType.bind(this)}>
                <h3 style={{ textAlign: "center" }}>Toggle Type {`<${this.props.tree.type}>`}</h3>
              </Btn>
              <Btn>
                <a href="https://github.com/ludw1gj/binary-fractal-tree">
                  <h3>GitHub</h3>
                </a>
              </Btn>
            </FlexRow>
          </FlexColumn>
        </FlexRow>
        <Canvas id="canvas" />
      </>
    );
  }
}

const mapStateToProps = state => ({ tree: state.tree });
const mapDispatchToProps = dispatch => {
  return {
    setTreeType: type => dispatch(setTreeType(type)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FractalTreeView);
