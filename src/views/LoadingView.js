import React, { useState, useLayoutEffect } from "react";
import PulseLoader from "react-spinners/PulseLoader";

const styles = {
  container: {
    width: "100vw",
    height: "100vh",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
  },
  loader: {
    marginTop: "100px",
  },
};

function LoadingView() {
  const [showLoader, setShowLoader] = useState(false);

  useLayoutEffect(() => {
    const timer = setTimeout(() => setShowLoader(true), 100);
    return () => {
      clearTimeout(timer);
    };
  }, []);

  return (
    <div style={styles.container}>
      <div style={styles.loader}>
        <PulseLoader sizeUnit={"px"} size={15} color={"#383838"} loading={showLoader} />
      </div>
    </div>
  );
}

export default LoadingView;
