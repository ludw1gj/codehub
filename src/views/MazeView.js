import React from "react";
import { Maze } from "@ludw1gj/maze-generation";
import { CanvasGrid } from "@ludw1gj/canvas-grid";
import styled from "styled-components";

import DocumentTitle from "components/DocumentTitle";
import ViewHeading from "components/ViewHeading";
import BackLink from "components/BackLink";
import Canvas from "generic/Canvas";
import FlexRow from "generic/FlexRow";
import FlexColumn from "generic/FlexColumn";

const Btn = styled.span`
  display: flex;
  flex-direction: row;
  width: 120px;
  justify-content: center;

  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  &:hover {
    color: green;
    cursor: pointer;
  }
`;

class MazeView extends React.Component {
  state = {
    maze: null,
    canvas: null,
  };

  componentDidMount() {
    const numRows = 30;
    const numCols = 50;
    const canvasOptions = {
      canvasId: "canvas",
      numRows,
      numCols,
      cellSize: 20,
      padding: 10,
    };
    const canvas = new CanvasGrid(canvasOptions);
    const maze = new Maze(numRows, numCols);
    const deps = { maze, canvas };
    this.setState({ ...deps });
    this.newMaze({ deps });
  }

  newMaze({ deps }) {
    const { maze, canvas } = deps ? deps : this.state;
    canvas.clean();
    maze.recursiveBacktracking();
    const painter = maze.generatePainter();
    canvas.paintCanvas(painter);
  }

  render() {
    return (
      <>
        <DocumentTitle>Maze Generation: Recursive Backtracking</DocumentTitle>
        <FlexRow>
          <FlexColumn style={{ flex: 3 }}>
            <ViewHeading>
              Maze Generation: Recursive Backtracking | <BackLink to="/">Go Back</BackLink>
            </ViewHeading>
          </FlexColumn>
          <FlexColumn style={{ flex: 2 }}>
            <FlexRow style={{ justifyContent: "flex-end" }}>
              <Btn onClick={this.newMaze.bind(this)}>
                <h3>New Maze</h3>
              </Btn>
              <Btn>
                <a href="https://github.com/ludw1gj/maze-generation">
                  <h3>GitHub</h3>
                </a>
              </Btn>
            </FlexRow>
          </FlexColumn>
        </FlexRow>

        <Canvas id="canvas" />
      </>
    );
  }
}

export default MazeView;
