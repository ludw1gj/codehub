import React from "react";
import { CanvasGrid } from "@ludw1gj/canvas-grid";
import { GameOfLife } from "@ludw1gj/game-of-life";
import styled from "styled-components";

import DocumentTitle from "components/DocumentTitle";
import ViewHeading from "components/ViewHeading";
import BackLink from "components/BackLink";
import Canvas from "generic/Canvas";
import FlexRow from "generic/FlexRow";
import FlexColumn from "generic/FlexColumn";

const Btn = styled.span`
  display: flex;
  flex-direction: row;
  width: 120px;
  justify-content: center;

  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  &:hover {
    color: green;
    cursor: pointer;
  }
`;

class GameOfLifeView extends React.Component {
  stopper = null;

  componentDidMount() {
    const numRows = 32;
    const numCols = 60;
    const framesPerSecond = 10;
    const canvas = new CanvasGrid({
      canvasId: "canvas",
      numRows,
      numCols,
      cellSize: 18,
      padding: 10,
    });
    const game = new GameOfLife({ numRows, numCols });

    this.stopper = { doStop: false };
    const painter = game.generatePainter(framesPerSecond, this.stopper);
    canvas.paintCanvas(painter);
  }

  componentWillUnmount() {
    this.stopper.doStop = true;
  }

  render() {
    return (
      <>
        <DocumentTitle>Conway's Game of Life</DocumentTitle>

        <FlexRow>
          <FlexColumn style={{ flex: 3 }}>
            <ViewHeading>
              Conway's Game of Life | <BackLink to="/">Go Back</BackLink>
            </ViewHeading>
          </FlexColumn>
          <FlexColumn style={{ flex: 2, alignItems: "flex-end" }}>
            <Btn>
              <a href="https://github.com/ludw1gj/conways-game-of-life">
                <h3>GitHub</h3>
              </a>
            </Btn>
          </FlexColumn>
        </FlexRow>

        <Canvas id="canvas" />
      </>
    );
  }
}

export default GameOfLifeView;
