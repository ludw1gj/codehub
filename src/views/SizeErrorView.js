import React from "react";

const styles = {
  container: {
    width: "100vw",
    height: "100vh",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    textAlign: "center",
    margin: "20px",
  },
};

function SizeErrorView() {
  return (
    <div style={styles.container}>
      <p style={styles.text}>
        Mobile Devices and Small Screen Sizes are not supported on this web app.
      </p>
    </div>
  );
}

export default SizeErrorView;
