import React, { useLayoutEffect, useState } from "react";
import { Link } from "react-router-dom";
import styled from "styled-components";

import DocumentTitle from "components/DocumentTitle";
import pikachu from "assets/images/pikachu.gif";
import Transition from "generic/Transition";

const List = styled.ul`
  li {
    margin-bottom: 5px;
  }
`;

const ProjectLink = styled(Link)`
  text-decoration: none;
  color: inherit;

  &:hover {
    color: green;
  }
`;

const Heading = styled.h2`
  margin-left: 15px;
`;

const Gif = styled.img`
  max-height: 240px;
  max-width: 360px;
  margin: 15px;
`;

function HomeView() {
  const [gifIsVisible, setGifIsVisible] = useState(false);
  useLayoutEffect(() => {
    setGifIsVisible(true);
  }, []);

  const toggleBoltCursor = () => {
    const boltCursorValue = 'url("/static/Lightning-Bolt.cur"), auto';
    const key = "--current-cursor";
    const value = getComputedStyle(document.documentElement).getPropertyValue(key);

    if (value === boltCursorValue) {
      document.documentElement.style.setProperty(key, "auto");
    } else {
      document.documentElement.style.setProperty(key, boltCursorValue);
    }
  };

  return (
    <>
      <DocumentTitle>Code Hub</DocumentTitle>
      <Heading>Code Hub</Heading>
      <List>
        <li>
          <ProjectLink to="/binary-fractal-tree">Binary Fractal Tree</ProjectLink>
        </li>
        <li>
          <ProjectLink to="/recursive-backtracking-maze">
            Recursive Backtracking Maze Generation
          </ProjectLink>
        </li>
        <li>
          <ProjectLink to="/conways-game-of-life">Conway's Game of Life</ProjectLink>
        </li>
        <li>
          <a href="https://mathfever.robertjeffs.com">MathFever (Etx.)</a>
        </li>
      </List>

      <Transition pose={gifIsVisible ? "visible" : "hidden"}>
        <Gif src={pikachu} onClick={toggleBoltCursor} />
      </Transition>
    </>
  );
}

export default HomeView;
