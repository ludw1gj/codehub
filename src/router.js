import React, { lazy, Suspense } from "react";
import { Switch, Route } from "react-router-dom";

import LoadingView from "./views/LoadingView";

const HomeView = lazy(() => import("./views/HomeView"));
const MazeView = lazy(() => import("./views/MazeView"));
const FractalTreeView = lazy(() => import("./views/FractalTreeView"));
const GameOfLifeView = lazy(() => import("./views/GameOfLifeView"));

function AppRouter() {
  return (
    <Suspense fallback={<LoadingView />}>
      <Switch>
        <Route exact path="/" component={HomeView} />
        <Route exact path="/binary-fractal-tree" component={FractalTreeView} />
        <Route exact path="/conways-game-of-life" component={GameOfLifeView} />
        <Route exact path="/recursive-backtracking-maze" component={MazeView} />
      </Switch>
    </Suspense>
  );
}

export default AppRouter;
